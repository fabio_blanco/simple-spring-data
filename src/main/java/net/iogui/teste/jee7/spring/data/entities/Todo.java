package net.iogui.teste.jee7.spring.data.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

/**
 * @author iogui (iogui@iogui.net)
 * @since 16/02/2018
 */
@Entity
@Table(name = "todos")
public class Todo implements Serializable {

	private static final long serialVersionUID = -4244423390612856727L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "creation_time", nullable = false)
	private Date creationTime;

	@Column(name = "description", length = 500)
	private String description;

	@Column(name = "modification_time")
	private Date modificationTime;

	@Column(name = "title", nullable = false, length = 100)
	private String title;

	@Version
	private long version;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Todo{");
		sb.append("id=").append(id);
		sb.append(", creationTime=").append(creationTime);
		sb.append(", description='").append(description).append('\'');
		sb.append(", modificationTime=").append(modificationTime);
		sb.append(", title='").append(title).append('\'');
		sb.append(", version=").append(version);
		sb.append('}');
		return sb.toString();
	}
}
