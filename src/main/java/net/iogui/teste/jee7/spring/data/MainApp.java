package net.iogui.teste.jee7.spring.data;

import net.iogui.teste.jee7.spring.data.service.Worker;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author iogui (iogui@iogui.net)
 * @since 25/10/2017
 */
public class MainApp {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

		Worker worker = ctx.getBean("workerImpl", Worker.class);
		worker.doIt(args);
	}
}
