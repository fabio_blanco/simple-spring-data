package net.iogui.teste.jee7.spring.data.service;

import net.iogui.teste.jee7.spring.data.entities.Todo;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author fabio.blanco (fabio.blanco@infracommerce.com.br)
 * @since 01/03/2018
 */
public interface TodoService {
	Todo save(Todo todo);

	Iterable<Todo> findAll();

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	void createDefaultTodo();

	@Transactional
	Long createTodo(String title, String description);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	Todo createTodoEstudarJava();

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	Todo createTodoEstudarSpringBoot();

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	Todo createTodoEstudarAngular();

	@Transactional
	void updateTodoDescription(Todo todo);

	@Transactional
	void testCreateFindPrintAndDelete();

	@Transactional
	void printAll();
}
