package net.iogui.teste.jee7.spring.data.repositories;

import net.iogui.teste.jee7.spring.data.entities.Todo;
import org.springframework.data.repository.CrudRepository;

/**
 * @author iogui (iogui@iogui.net)
 * @since 01/03/2018
 */
public interface TodoCrudRepository extends CrudRepository<Todo, Long> {
}
