package net.iogui.teste.jee7.spring.data.service.impl;

import net.iogui.teste.jee7.spring.data.entities.Todo;
import net.iogui.teste.jee7.spring.data.repositories.TodoCrudRepository;
import net.iogui.teste.jee7.spring.data.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author fabio.blanco (fabio.blanco@infracommerce.com.br)
 * @since 01/03/2018
 */
@Service
public class TodoServiceImpl implements TodoService {

	@Autowired
	private TodoCrudRepository todoCrudRepository;

	@Transactional
	@Override
	public Todo save(Todo todo) {
		return todoCrudRepository.save(todo);
	}

	@Transactional
	@Override
	public Iterable<Todo> findAll() {
		return todoCrudRepository.findAll();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public void createDefaultTodo() {
		Todo todo = new Todo();
		todo.setTitle("Default TODO");
		todo.setDescription("TODO default de teste");
		todo.setCreationTime(new Date());

		todoCrudRepository.save(todo);
	}

	@Transactional
	@Override
	public Long createTodo(String title, String description) {
		Todo todo = new Todo();
		todo.setTitle(title);
		todo.setDescription(description);
		todo.setCreationTime(new Date());

		Todo created = todoCrudRepository.save(todo);

		if (created != null) {
			return created.getId();
		} else {
			return null;
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public Todo createTodoEstudarJava() {
		System.out.println("Criando TODO \"Estudar Java\"...");
		Long id = createTodo("Estudar Java", "Estudar java 7, 8 e 9 e suas diferenças");
		System.out.println("...Criado.");

		System.out.println();

		System.out.println("Buscando usando o id: " + id);

		Todo todo = todoCrudRepository.findOne(id);

		return todo;

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public Todo createTodoEstudarSpringBoot() {
		System.out.println("Criando TODO \"Estudar SpringBoot\"...");
		Long id = createTodo("Estudar SpringBoot", "Estudar SpringBoot");
		System.out.println("...Criado.");

		System.out.println();

		System.out.println("Buscando usando o id: " + id);

		Todo todo = todoCrudRepository.findOne(id);

		return todo;

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public Todo createTodoEstudarAngular() {
		System.out.println("Criando TODO \"Estudar Angular\"...");
		Long id = createTodo("Estudar Angular", "Estudar Angular");
		System.out.println("...Criado.");

		System.out.println();

		System.out.println("Buscando usando o id: " + id);

		Todo todo = todoCrudRepository.findOne(id);

		return todo;

	}

	@Transactional
	@Override
	public void updateTodoDescription(Todo todo) {
		todo.setDescription(todo.getDescription() + ". Mas estudar com afinco!! ");
		todoCrudRepository.save(todo);
	}

	@Transactional
	@Override
	public void testCreateFindPrintAndDelete() {
		printAll();

		Todo todo = createTodoEstudarJava();

		System.out.println(todo);

		Long id = todo.getId();

		printAll();

		System.out.println("Removendo TODO de id: " + id);

		todoCrudRepository.delete(id);

		printAll();

	}

	@Transactional
	@Override
	public void printAll() {
		Iterable<Todo> iterable = todoCrudRepository.findAll();

		System.out.println();
		System.out.println("Printando tudo: ");
		for (Todo todo : iterable) {
			System.out.println(todo);
		}

		System.out.println();
	}

}
