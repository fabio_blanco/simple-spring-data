package net.iogui.teste.jee7.spring.data.service;

import org.springframework.stereotype.Component;

/**
 * @author iogui (iogui@iogui.net)
 * @since 25/10/2017
 */
public interface Worker {
	void doIt(String[] args);
}
