package net.iogui.teste.jee7.spring.data.service.impl;

import net.iogui.teste.jee7.spring.data.entities.Todo;
import net.iogui.teste.jee7.spring.data.service.TodoService;
import net.iogui.teste.jee7.spring.data.service.Worker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author iogui (iogui@iogui.net)
 * @since 25/10/2017
 */
@Component
@Qualifier("worker")
public class WorkerImpl implements Worker {

	@Autowired
	private TodoService todoService;

	@Override
	public void doIt(String[] args) {
		System.out.println("Criando um TODO default:");

		todoService.createDefaultTodo();

		System.out.println("... criado.");

		System.out.println("Listando:");

		Iterable<Todo> todos = todoService.findAll();
		for (Todo todo : todos) {
			System.out.println(todo);
		}

		System.out.println();

		todoService.testCreateFindPrintAndDelete();

		System.out.println();

		todoService.createTodoEstudarJava();
		todoService.createTodoEstudarSpringBoot();
		Todo estudarAngular = todoService.createTodoEstudarAngular();

		todoService.printAll();

		System.out.println("Atualizando o TODO estudarAngular...");

		todoService.updateTodoDescription(estudarAngular);

		todoService.printAll();
	}
}
