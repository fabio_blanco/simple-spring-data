package net.iogui.teste.jee7.spring.data.repositories;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * @author iogui (iogui@iogui.net)
 * @since 05/03/2018
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends Repository<T, ID> {
	void delete(T deleted);
	List<T> findAll();
	T findOne(ID id);
	T save(T persisted);
}
